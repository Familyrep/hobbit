#######################################################################
 # ----------------------------------------------------------------------------
 # "THE BEER-WARE LICENSE" (Revision 42):
 # @tantrumdev wrote this file.  As long as you retain this notice you
 # can do whatever you want with this stuff. If we meet some day, and you think
 # this stuff is worth it, you can buy me a beer in return. - Muad'Dib
 # ----------------------------------------------------------------------------
#######################################################################

#######################################################################
# Import Modules Section
import xbmc, xbmcaddon, xbmcgui, xbmcplugin, base64
import os
#######################################################################

#######################################################################
# Set this to True to see the menu on non-android devices for dev work
DEVELOPER           = True
#######################################################################

#######################################################################
# Primary Addon Variables
AddonID             = xbmcaddon.Addon().getAddonInfo('id')
ADDON               = xbmcaddon.Addon(id=AddonID)
HOME                = xbmc.translatePath('special://home/')
ADDONS              = os.path.join(HOME, 'addons')
USERDATA            = os.path.join(HOME, 'userdata')
ADDON_DATA          = xbmc.translatePath(os.path.join(USERDATA, 'addon_data'))
ownAddon            = xbmcaddon.Addon(id=AddonID)
URL                 = base64.b64decode(b'aHR0cDovL29tbGFuZHNjYXBpbmcuY28udWsvYXBraW5zdGFsbGVyL3RleHQv')
NEWSURL             = base64.b64decode(b'aHR0cDovL29tbGFuZHNjYXBpbmcuY28udWsvYXBraW5zdGFsbGVyL25ld3MueG1s')
ADDONTITLE          = base64.b64decode(b'W0NPTE9SIGxpbWVdW0JdRmFtaWx5Wy9CXVsvQ09MT1JdIFtDT0xPUiBzbm93XVtCXUFQSyBJbnN0YWxsZXJbL0JdWy9DT0xPUl0=')
#######################################################################

#######################################################################
# Filename Variables 
BASEURL             = URL
EMU_FILE            = BASEURL + base64.b64decode(b'RW11bGF0b3JzLnR4dA==')
KODI_FILE           = BASEURL + base64.b64decode(b'S29kaS50eHQ=')
LIVETV_FILE         = BASEURL + base64.b64decode(b'TGl2ZXR2LnR4dA==')
VOD_FILE            = BASEURL + base64.b64decode(b'Vm9kLnR4dA==')
MUSIC_FILE          = BASEURL + base64.b64decode(b'TXVzaWMudHh0')
TOOLS_FILE          = BASEURL + base64.b64decode(b'VG9vbHMudHh0')
#######################################################################

#######################################################################
# Theme Variables
FONTHEADER          = base64.b64decode(b'Rm9udDE0')
FANART              = xbmc.translatePath(os.path.join('special://home/addons/' + AddonID, 'fanart.jpg'))
ICON                = xbmc.translatePath(os.path.join('special://home/addons/' + AddonID, 'icon.png'))
ART                 = xbmc.translatePath(os.path.join('special://home/addons/' + AddonID, 'resources/art/'))
#######################################################################

#######################################################################
ADDONDATA           = os.path.join(USERDATA, 'addon_data', AddonID)
dialog              = xbmcgui.Dialog()
DIALOG              = xbmcgui.Dialog()
dp                  = xbmcgui.DialogProgress()
DP                  = xbmcgui.DialogProgress()
LOG                 = xbmc.translatePath('special://logpath/')
PLUGIN              = os.path.join(ADDONS, AddonID)
skin                = xbmc.getSkinDir()
USER_AGENT          = base64.b64decode(b'TW96aWxsYS81LjAgKFdpbmRvd3M7IFU7IFdpbmRvd3MgTlQgNS4xOyBlbi1HQjsgcnY6MS45LjAuMykgR2Vja28vMjAwODA5MjQxNyBGaXJlZm94LzMuMC4z')
#######################################################################